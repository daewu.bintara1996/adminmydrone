package com.admindrone.modules.Login;

import android.content.Context;

import com.admindrone.app.App;
import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.Login;

import java.util.HashMap;

public class LoginPagePresenter extends BaseRxLcePresenter<XLoginPageView, Login>
        implements XLoginPagePresenter {

    public LoginPagePresenter(Context context) {
        super(context);
    }

    public void getOtpCode(String noHp) {
        HashMap<String, String> map = new HashMap<>();
        map.put("act", "login");
        map.put("phone", noHp);
        subscribe(App.getService().apiLoginRequest(map), false);
    }
}