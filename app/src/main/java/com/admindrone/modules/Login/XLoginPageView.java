package com.admindrone.modules.Login;

import com.admindrone.models.Login;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XLoginPageView extends MvpLceView<Login> {

}