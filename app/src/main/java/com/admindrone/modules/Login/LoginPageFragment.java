package com.admindrone.modules.Login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.admindrone.R;
import com.admindrone.base.BaseMvpLceFragment;
import com.admindrone.models.Login;
import com.admindrone.modules.OtpPage.OtpPageFragmentBuilder;
import com.admindrone.modules.Register.RegisterFragment;
import com.admindrone.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginPageFragment extends BaseMvpLceFragment<RelativeLayout, Login, XLoginPageView, LoginPagePresenter>
        implements XLoginPageView {

    @BindView(R.id.etNoHp)
    EditText etNoHp;
    @BindView(R.id.btnNext)
    Button btnNext;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.fHome)
    FrameLayout fHome;
    @BindView(R.id.fPoint)
    FrameLayout fPoint;

    public LoginPageFragment() {

    }

    @Override
    public LoginPagePresenter createPresenter() {
        return new LoginPagePresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_login_page;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
        showContent();
        loadingView.setVisibility(View.GONE);

        CommonUtilities.finishActivity(view, getActivity());

        onActionButton(view);

    }

    @OnClick({R.id.btnNext, R.id.btnRegister})
    public void onActionButton(View view) {
        switch (view.getId()){
            case R.id.btnRegister :
                CommonUtilities.hideSoftKeyboard(getActivity());
                mListener.gotoPage(new RegisterFragment(), true, "register");
                break;
            case R.id.btnNext :
                CommonUtilities.hideSoftKeyboard(getActivity());
                loadData(false);
                break;
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
        return e.getMessage();
    }

    @Override
    public void setData(Login data) {
        mListener.gotoPage(new OtpPageFragmentBuilder(data.getAuthorId()+"").build(), true, "otp");
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        if (!etNoHp.getText().toString().isEmpty() && etNoHp.getText().toString().length()>9){
            presenter.getOtpCode(etNoHp.getText().toString());
        }
    }
}