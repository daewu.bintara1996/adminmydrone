package com.admindrone.modules.Home;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.admindrone.R;
import com.admindrone.adapter.DronelistAdapter;
import com.admindrone.base.BaseLceRefreshFragment;
import com.admindrone.models.RequestDrone;
import com.admindrone.modules.AddNewDrone.NewDroneFragmentBuilder;
import com.admindrone.modules.DroneReslut.ResultListFragmentBuilder;
import com.admindrone.modules.MainActivity;
import com.admindrone.modules.SetupDrone.SetupDroneFragmentBuilder;
import com.admindrone.modules.SplashScreen.SplashScreenFragment;
import com.admindrone.utility.CommonUtilities;
import com.admindrone.utility.FunctionMedia;
import com.admindrone.utility.Preferences;
import com.admindrone.utility.recyclerview.EndlessRecyclerOnScrollListener;
import com.admindrone.utility.recyclerview.ItemClickSupport;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static android.app.Activity.RESULT_OK;
import static com.admindrone.utility.Config.INTENT_CAMERA;
import static com.admindrone.utility.Config.INTENT_GALLERY;

@FragmentWithArgs
public class HomeFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestDrone, XHomeView, HomePresenter>
        implements XHomeView {

    @Arg
    String author_id;
    @BindView(R.id.rvDrone)
    RecyclerView rvDrone;
    @BindView(R.id.contentView)
    SwipeRefreshLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.imgAuthor)
    CircularImageView imgAuthor;
    @BindView(R.id.tvAuthorName)
    TextView tvAuthorName;
    @BindView(R.id.btnLogout)
    Button btnLogout;
    @BindView(R.id.btnAddNewDrone)
    Button btnAddNewDrone;
    @BindView(R.id.btnEditGambar)
    Button btnEditGambar;
    @BindView(R.id.rl12)
    RelativeLayout rl12;
    private DronelistAdapter dronelistAdapter = new DronelistAdapter();
    EndlessRecyclerOnScrollListener scrollListener;
    private AlertDialog alertDialog;
    Uri uriImage;
    private String reg_id;
    private String action, IMEI;
    private Integer position_item = 0;

    public HomeFragment() {

    }

    @Override
    public HomePresenter createPresenter() {
        return new HomePresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContent();
        CommonUtilities.finishActivity(view, getActivity());
        onClickFunction(view);
        CommonUtilities.hideSoftKeyboard(getActivity());
        reg_id = FirebaseInstanceId.getInstance().getToken();
        IMEI = CommonUtilities.getIMEI(getContext());

        // location permission
        try {
            if (ContextCompat.checkSelfPermission(getContext().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);

        scrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadData(false);
            }
        };
        rvDrone.addOnScrollListener(scrollListener);
        rvDrone.setHasFixedSize(true);
        rvDrone.setLayoutManager(layoutManager);
        rvDrone.setAdapter(dronelistAdapter);
        dronelistAdapter.clear();

        // item click action
        ItemClickSupport.addTo(rvDrone).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (dronelistAdapter.getItem(position).getDroneOnlineStatus().equals("online") && dronelistAdapter.getItem(position).getDroneStatus().equals("nontested")) {
                    mListener.gotoPage(new SetupDroneFragmentBuilder(author_id,
                            dronelistAdapter.getItem(position).getDroneId() + "",
                            dronelistAdapter.getItem(position).getDroneImage() + "",
                            dronelistAdapter.getItem(position).getDroneName() + ""
                    ).build(), true, "setup");
                } else if (dronelistAdapter.getItem(position).getDroneStatus().equals("tested")) {
                    sweetAlertDialogSweet("DRONE TESTED!","Ingin test lagi?",
                            "Lihat hasil", "Test lagi", true,SweetAlertDialog.SUCCESS_TYPE)
                            .setCancelClickListener(sweetAlertDialog -> {sweetAlertDialog.dismissWithAnimation();
                                if (dronelistAdapter.getItem(position).getDroneOnlineStatus().equals("online")) {
                                    mListener.gotoPage(new SetupDroneFragmentBuilder(author_id, dronelistAdapter.getItem(position).getDroneId() + "",
                                            dronelistAdapter.getItem(position).getDroneImage() + "", dronelistAdapter.getItem(position).getDroneName() + ""
                                    ).build(), true, "setup");
                                } else {
                                    showToast("Drone Offline, ubah ke mode Online untuk memulai!");
                                }
                            })
                            .setConfirmClickListener(sweetAlertDialog->{sweetAlertDialog.dismissWithAnimation();
                                mListener.gotoPage(new ResultListFragmentBuilder(dronelistAdapter.getItem(position).getDroneId() + "",
                                        dronelistAdapter.getItem(position).getDroneImage() + "", dronelistAdapter.getItem(position).getDroneName() + ""
                                ).build(), false, "result");
                            }).show();
                } else {
                    showToast("Drone Offline, ubah ke mode Online untuk memulai!");
                }
            }
        });

        //item long click
        ItemClickSupport.addTo(rvDrone).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                sweetAlertDialogSweet("HAPUS DRONE","Yakin untuk menghapus "+dronelistAdapter.getItem(position).getDroneName()+"?",
                        "Hapus", "Batal", true,SweetAlertDialog.WARNING_TYPE)
                        .setCancelClickListener(sweetAlertDialog1 -> sweetAlertDialog1.dismissWithAnimation())
                        .setConfirmClickListener(sweetAlertDialog->{ sweetAlertDialog.dismissWithAnimation();
                            action = "delete_drone";
                            position_item = position;
                            presenter.deleteDrone(dronelistAdapter.getItem(position).getDroneId()+"");
                        }).show();
                return false;
            }
        });

        action = "saveRegId";
        getPresenter().saveRegID(author_id, reg_id);

    }

    @OnClick({R.id.btnLogout, R.id.imgAuthor, R.id.btnAddNewDrone, R.id.btnEditGambar})
    public void onClickFunction(View view) {
        switch (view.getId()) {
            case R.id.btnLogout:
                sweetAlertDialogSweet("LOGOUT","Yakin untuk keluar akun?", "Logout", "Tidak", true, SweetAlertDialog.WARNING_TYPE)
                        .setCancelClickListener(sweetAlertDialog1 -> sweetAlertDialog1.dismissWithAnimation())
                        .setConfirmClickListener(sweetAlertDialog->{sweetAlertDialog.dismissWithAnimation();
                            Preferences preferences = new Preferences(getContext());
                            preferences.clearAllPreferences();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent); getActivity().finish();
                        }).show();
                break;
            case R.id.btnEditGambar:
                final RxPermissions rxPermissions = new RxPermissions(getActivity());
                PopupMenu popupMenu = new PopupMenu(getContext(), imgAuthor);
                popupMenu.inflate(R.menu.menu_upload);
                popupMenu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.mn_camera:
                            rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .subscribe(new Observer<Boolean>() {

                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            if (aBoolean) {
                                                uriImage = FunctionMedia.Camera(HomeFragment.this, INTENT_CAMERA);
                                            } else {
                                                showToast("Gagal membuka gambar");
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            showToast("Gagal membuka gambar");
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                            break;
                        case R.id.mn_gallery:
                            rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .subscribe(new Observer<Boolean>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            if (aBoolean) {
                                                FunctionMedia.BrowseImage(HomeFragment.this, INTENT_GALLERY);
                                            } else {
                                                showToast("Permission denied");
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            showToast("Permission denied");
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                            break;
                    }
                    return false;
                });
                popupMenu.show();
                break;

            case R.id.btnAddNewDrone:
                mListener.gotoPage(new NewDroneFragmentBuilder(author_id).build(), true, "addnew");
                break;
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(RequestDrone data) {
        switch (action) {
            case "loadDrone":
                dronelistAdapter.setItems(data.getDroneList());
                tvAuthorName.setText(data.getAuthorName());
                Picasso.get()
                        .load(data.getAuthorImage() + "")
                        .into(imgAuthor);
                if (data.getAuthorImage() == null){
                    imgAuthor.setBackgroundResource(R.drawable.btn_edit_circle);
                }
                break;
            case "saveRegId":
                loadData(false);
                break;
            case "uploadImage":
                Picasso.get().load(uriImage).into(imgAuthor);
                break;
            case "delete_drone":
                dronelistAdapter.removeItem(position_item);
                break;
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        action = "loadDrone";
        if (pullToRefresh) {
            dronelistAdapter.clear();
            contentView.setRefreshing(false);
        }
        int page = dronelistAdapter.getItemCount();
        getPresenter().onLoadDrone(author_id, pullToRefresh, page);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == INTENT_CAMERA) {
                CropImage.activity(uriImage)
                        .setAspectRatio(4, 4)
                        .setFixAspectRatio(true)
                        .start(getContext(), HomeFragment.this);
            } else if (requestCode == INTENT_GALLERY) {
                uriImage = data.getData();
                CropImage.activity(uriImage)
                        .setAspectRatio(4, 4)
                        .setFixAspectRatio(true)
                        .start(getContext(), HomeFragment.this);
            }
        } else {
            if (requestCode == INTENT_CAMERA) {
                showLightError("Gagal mengambil gambar.");
            } else if (requestCode == INTENT_GALLERY) {
                showLightError("Gagal mengambil gambar.");
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriImage = result.getUri();
                File f = new File(uriImage.getPath());
                action = "uploadImage";
                presenter.uploadImage(author_id, f);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                showLightError(error.getMessage());
            }
        }
    }
}