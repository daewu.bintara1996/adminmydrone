package com.admindrone.modules.Home;

import android.content.Context;

import com.admindrone.app.App;
import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.RequestDrone;
import com.admindrone.utility.CommonUtilities;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class HomePresenter extends BaseRxLcePresenter<XHomeView, RequestDrone>
        implements XHomePresenter {

    public HomePresenter(Context context) {
        super(context);
    }

    public void onLoadDrone(String author_id, boolean pullToRefresh, int page) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "drone_by_author");
        params.put("drone_author_id", author_id);
        params.put("page", page+"");
        subscribe(App.getService().apiDroneRequest(params));
    }

    public void saveRegID(String author_id, String reg_id) {
        Map<String, String> map = new HashMap<>();
        map.put("act", "update_reg_id");
        map.put("author_reg_id", reg_id);
        map.put("author_id", author_id);
        map.put("device_name", CommonUtilities.getDeviceName());
        map.put("device_os", CommonUtilities.getOsVersionName());
        subscribe(App.getService().apiRequestSaveRegId(map),false);
    }

    public void uploadImage(String author_id, File f) {
        RequestBody act = RequestBody.create(MediaType.parse("text/plain"), "update_image");
        RequestBody author_idrb = RequestBody.create(MediaType.parse("text/plain"), author_id);

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), f);
        MultipartBody.Part file = MultipartBody.Part.createFormData("author_image", f.getName(), requestBody);

        subscribe(App.getService().apiUpdateImage(act, author_idrb, file), false);
    }

    public void deleteDrone(String drone_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("act", "delete_drone");
        map.put("drone_id", drone_id);
        subscribe(App.getService().apiDroneDelete(map));

    }
}