package com.admindrone.modules.SetupDrone;

import android.content.Context;

import com.admindrone.app.App;
import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.ABase.RequestBase;

import java.util.HashMap;

public class SetupDronePresenter extends BaseRxLcePresenter<XSetupDroneView, RequestBase>
        implements XSetupDronePresenter {

    public SetupDronePresenter(Context context) {
        super(context);
    }

    public void addResult(String speedAverage, String bateryAwal, String bateryAkhir, String author_id, String drone_id, String timer) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "new_result");
        params.put("drone_author_id", author_id);
        params.put("drone_id", drone_id+"");
        params.put("speed_average", speedAverage+"");
        params.put("batery_start", bateryAwal);
        params.put("batery_stop", bateryAkhir);
        params.put("timer_test", timer);
        subscribe(App.getService().apiDroneNewResult(params), true);
    }

    public void checkResult(String drone_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("act", "check_online_drone");
        map.put("drone_id", drone_id);
        subscribe(App.getService().apiDroneNewResult(map));
    }
}