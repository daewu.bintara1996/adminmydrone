package com.admindrone.modules.SetupDrone;

import com.admindrone.models.ABase.RequestBase;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XSetupDroneView extends MvpLceView<RequestBase> {

}