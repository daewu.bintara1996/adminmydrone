package com.admindrone.modules.SetupDrone;

import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentManager;

import com.admindrone.R;
import com.admindrone.base.BaseMvpLceFragment;
import com.admindrone.models.ABase.RequestBase;
import com.admindrone.models.Speed;
import com.admindrone.models.SpeedList;
import com.admindrone.modules.Home.HomeFragmentBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.admindrone.utility.CommonUtilities.hideView;

@FragmentWithArgs
public class SetupDroneFragment extends BaseMvpLceFragment<RelativeLayout, RequestBase, XSetupDroneView, SetupDronePresenter>
        implements XSetupDroneView {

    @Arg
    String author_id;
    @Arg
    String drone_image;
    @Arg
    String drone_name;
    @Arg
    String drone_id;
    @BindView(R.id.btnBack)
    Button btnBack;
    @BindView(R.id.tvTitleDroneBar)
    AppCompatTextView tvTitleDroneBar;
    @BindView(R.id.rlBar)
    RelativeLayout rlBar;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;

    MapboxMap map;
    @BindView(R.id.etBateryAwal)
    TextInputEditText etBateryAwal;
    @BindView(R.id.etKecepatan)
    TextInputEditText etKecepatan;
    @BindView(R.id.etBateryAkhir)
    TextInputEditText etBateryAkhir;
    @BindView(R.id.btnSaveAnalisis)
    Button btnSaveAnalisis;
    @BindView(R.id.tvSpeed)
    AppCompatTextView tvSpeed;
    @BindView(R.id.tvTimer)
    AppCompatTextView tvTimer;
    @BindView(R.id.btnStartTimer)
    ToggleButton btnStartTimer;
    @BindView(R.id.spinner)
    AppCompatSpinner spinner;
    @BindView(R.id.spinnerAkhir)
    AppCompatSpinner spinnerAkhir;
    @BindView(R.id.rlspinnerAwal)
    RelativeLayout rlspinnerAwal;
    @BindView(R.id.rlspinnerAkhir)
    RelativeLayout rlspinnerAkhir;
    private CameraPosition cameraPosition;
    private LatLng point;
    private FirebaseDatabase mFirebaseInstance;
    private DatabaseReference mFirebaseDatabase;
    private String latitude, longitude, sTimer, action;
    private Handler handler;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    int Seconds, Minutes, MilliSeconds;
    private ArrayAdapter<CharSequence> spinnerAdapter;
    private Speed speedSnapshoot;
    private ArrayList<SpeedList> speeds;
    private ValueEventListener mFirebaseListener;
    private SpeedList speedList;
    private double sumSpeed = 0.0;
    BroadcastReceiver getmBatInfoReceiver;

    public SetupDroneFragment() {

    }

    @Override
    public SetupDronePresenter createPresenter() {
        return new SetupDronePresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_setup_drone;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContent();
        hideView(rlspinnerAwal);
        hideView(rlspinnerAkhir);

        speeds = new ArrayList<>();
        spinnerAdapter = ArrayAdapter.createFromResource(getContext(), R.array.spinner_array, android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(spinnerAdapter);
        spinnerAkhir.setAdapter(spinnerAdapter);

        handler = new Handler();

        tvTitleDroneBar.setText(drone_name);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        map = mapboxMap;
                        enableLocationComponent(style);
                    }
                });
            }
        });

        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'mydrone' node
        mFirebaseDatabase = mFirebaseInstance.getReference("mydrone");

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    onNotifBack();
                    return true;
                }
                return false;
            }
        });

        btnStartTimer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                } else {
                    TimeBuff += MillisecondTime;
                    handler.removeCallbacks(runnable);
//                showToast(speeds.size()-1+"\n"+speeds.get(speeds.size()-1).getSpeed().substring(0, speedList.getSpeed().length()-5)+"\nsum : "+sumSpeed+"\n"+
//                        "average : "+sumSpeed/speeds.size());
                    etKecepatan.setText(sumSpeed/speeds.size()+" Km/h");
                }
            }
        });

        speedList = new SpeedList();

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(RequestBase data) {
        showToast(data.getResultAdd());
        switch (action) {
            case "toSave":
                if (data.getResultAdd().equals("success")) {
                    mFirebaseDatabase.onDisconnect();
                    mListener.gotoPage(new HomeFragmentBuilder(author_id).build(), true, "home");
                } else {
                    sweetAlertDialogSweet("PERINGATAN!", "Stop proses terlebih dahulu pada aplikasi My Drone untuk menyimpan hasil.",
                            "Oke", null, false, SweetAlertDialog.WARNING_TYPE)
                            .setConfirmClickListener(sweetAlertDialog -> {
                                sweetAlertDialog.dismissWithAnimation();
                            }).show();
                }
                break;
            case "toBack":
                if (data.getResultAdd().equals("online")) {
                    showToast("Drone masih online, ubah ke mode offline untuk kembali!");
                } else {
                    mFirebaseDatabase.onDisconnect();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                }
                break;
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    /**
     * Initialize the Maps SDK's LocationComponent
     */
    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
//        mFirebaseDatabase.keepSynced(true);
        LocationComponent locationComponent = map.getLocationComponent();
        LocationComponentActivationOptions locationComponentActivationOptions = LocationComponentActivationOptions.builder(getContext(), loadedMapStyle).useDefaultLocationEngine(false).build();
        locationComponent.activateLocationComponent(locationComponentActivationOptions);
        locationComponent.setLocationComponentEnabled(true);
        locationComponent.setCameraMode(CameraMode.TRACKING);
        locationComponent.setRenderMode(RenderMode.COMPASS);
        mFirebaseListener = mFirebaseDatabase.child("drone_id_" + drone_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                speedSnapshoot = dataSnapshot.getValue(Speed.class);
                tvSpeed.setText(speedSnapshoot.speed);
                longitude = speedSnapshoot.longitude;
                latitude = speedSnapshoot.latitude;
                point = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                cameraPosition = new CameraPosition.Builder().target(point).build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                IconFactory iconFactory = IconFactory.getInstance(getContext());
                Icon icon = iconFactory.fromResource(R.drawable.icon_drone_64px);
                map.addMarker(new MarkerOptions().position(point).title(drone_name.toUpperCase()).icon(icon));

                speedList.setSpeed(speedSnapshoot.speed);
                speedList.setDoubleSpeed(Double.parseDouble(speedSnapshoot.getSpeed().substring(0, speedList.getSpeed().length() - 5)));
                speeds.add(speedList);
                //get sumSpeed
                sumSpeed = sumSpeed + Double.parseDouble(speedSnapshoot.getSpeed().substring(0, speedList.getSpeed().length() - 5));
                etBateryAwal.setText(speedSnapshoot.batteryStart);
                etBateryAkhir.setText(speedSnapshoot.batteryStop);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @OnClick({R.id.btnBack, R.id.btnSaveAnalisis})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onNotifBack();
                break;
            case R.id.btnSaveAnalisis:
                TimeBuff += MillisecondTime;
                handler.removeCallbacks(runnable);
                btnStartTimer.setChecked(false);
                if (!etKecepatan.getText().toString().isEmpty() && !etBateryAwal.getText().toString().isEmpty() && !etBateryAkhir.getText().toString().isEmpty() && !tvTimer.getText().toString().equals("00.00.00")) {
                    action = "toSave";
                    presenter.addResult(etKecepatan.getText().toString(),
                            etBateryAwal.getText().toString(), etBateryAkhir.getText().toString(),
//                            etBateryAwal.getText().toString()+" "+spinner.getSelectedItem().toString(),
//                            etBateryAkhir.getText().toString()+" "+spinnerAkhir.getSelectedItem().toString(),
                            author_id, drone_id, sTimer
                    );
                } else {
                    showToast("Tidak bisa menyimpan, data dan proses belum lengkap!");
                }
                break;
        }
    }

    private void onNotifBack() {
        sweetAlertDialogSweet("BATAL TESTING", "Yakin untuk membatalkan testing?",
                "Ya", "Tidak", true, SweetAlertDialog.WARNING_TYPE)
                .setCancelClickListener(sweetAlertDialog1 -> sweetAlertDialog1.dismissWithAnimation())
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlertDialog.dismissWithAnimation();
                    action = "toBack";
                    presenter.checkResult(drone_id);
                }).show();
    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;
            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);
            tvTimer.setText("" + Minutes + ":"
                    + String.format("%02d", Seconds) + ":"
                    + String.format("%03d", MilliSeconds));

            sTimer = Minutes + " menit, " + String.format("%02d", Seconds) + " detik";

            handler.postDelayed(this, 0);
        }
    };

    @Override
    public void onStop() {
        mFirebaseDatabase.onDisconnect();
        super.onStop();
    }
}