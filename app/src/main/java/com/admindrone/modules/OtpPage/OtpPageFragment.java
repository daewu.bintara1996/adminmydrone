package com.admindrone.modules.OtpPage;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.admindrone.R;
import com.admindrone.base.BaseMvpLceFragment;
import com.admindrone.models.VerivyOtp;
import com.admindrone.modules.Home.HomeFragmentBuilder;
import com.admindrone.utility.CommonUtilities;
import com.admindrone.utility.Config;
import com.admindrone.utility.Preferences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;
import butterknife.OnClick;

import static com.admindrone.utility.Config.TOKEN;

@FragmentWithArgs
public class OtpPageFragment extends BaseMvpLceFragment<RelativeLayout, VerivyOtp, XOtpPageView, OtpPagePresenter>
        implements XOtpPageView {

    @Arg String author_id;

    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.etKodeOtp)
    EditText etKodeOtp;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnResendOtp)
    Button btnResendOtp;
    @BindView(R.id.lnView)
    LinearLayout lnView;
    @BindView(R.id.contentView)
    RelativeLayout contentView;

    private AlertDialog customDialog ;
    Handler handler = new Handler();
    String reg_id;

    public OtpPageFragment() {

    }

    @Override
    public OtpPagePresenter createPresenter() {
        return new OtpPagePresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_otp_page;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadingView.setVisibility(View.GONE);
        showContent();
        CommonUtilities.finishActivity(view, getActivity());
        onButtonClick(view);
        reg_id = FirebaseInstanceId.getInstance().getToken();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lnView.setVisibility(View.VISIBLE);
            }
        }, 80000);

    }

    @OnClick({R.id.btnLogin, R.id.btnResendOtp})
    public void onButtonClick(View view) {
        switch (view.getId()){
            case R.id.btnLogin :
                if (!etKodeOtp.getText().toString().isEmpty() && etKodeOtp.getText().toString().length() == 6){
                    loadData(false);
                }
                break;
            case R.id.btnResendOtp :
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                CommonUtilities.hideSoftKeyboard(getActivity());
                break;
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
        return e.getMessage();
    }

    @Override
    public void setData(VerivyOtp data) {
        CommonUtilities.hideSoftKeyboard(getActivity());
        handler.removeCallbacksAndMessages(null);
        Preferences preferences = new Preferences(getContext());
        preferences.savePreferences("TOKEN", reg_id);
        preferences.savePreferences("author_id", author_id);
        mListener.gotoPage(new HomeFragmentBuilder(author_id).build(), true, "home");
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.onCheckOtp(author_id, etKodeOtp.getText().toString());
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}