package com.admindrone.modules.OtpPage;

import android.content.Context;

import com.admindrone.app.App;
import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.VerivyOtp;

import java.util.HashMap;

public class OtpPagePresenter extends BaseRxLcePresenter<XOtpPageView, VerivyOtp>
        implements XOtpPagePresenter {

    public OtpPagePresenter(Context context) {
        super(context);
    }

    public void onCheckOtp(String author_id, String otp) {
        HashMap<String, String> map = new HashMap<>();
        map.put("act", "verify_otp");
        map.put("author_id", author_id);
        map.put("otp_code", otp);
        subscribe(App.getService().apiVerifyOtpRequest(map), false);
    }
}