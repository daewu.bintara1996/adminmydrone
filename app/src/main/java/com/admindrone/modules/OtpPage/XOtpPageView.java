package com.admindrone.modules.OtpPage;

import com.admindrone.models.VerivyOtp;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XOtpPageView extends MvpLceView<VerivyOtp> {

}