package com.admindrone.modules.DroneReslut;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.admindrone.R;
import com.admindrone.adapter.ResultDroneAdapter;
import com.admindrone.base.BaseLceRefreshFragment;
import com.admindrone.models.ABase.RequestBase;
import com.admindrone.modules.SplashScreen.SplashScreenFragment;
import com.admindrone.utility.CommonUtilities;
import com.admindrone.utility.Preferences;
import com.admindrone.utility.recyclerview.EndlessRecyclerOnScrollListener;
import com.admindrone.utility.recyclerview.ItemClickSupport;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;
import cn.pedant.SweetAlert.SweetAlertDialog;


@FragmentWithArgs
public class ResultListFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestBase, XResultListView, ResultListPresenter>
        implements XResultListView {

    @Arg
    String drone_id;
    @Arg
    String drone_name;
    @Arg
    String drone_image;
    @BindView(R.id.btnBack)
    Button btnBack;
    @BindView(R.id.tvDroneName)
    AppCompatTextView tvDroneName;
    @BindView(R.id.rlBar)
    RelativeLayout rlBar;
    @BindView(R.id.rvResultDrone)
    RecyclerView rvResultDrone;
    @BindView(R.id.contentView)
    SwipeRefreshLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;

    private ResultDroneAdapter resAdapter = new ResultDroneAdapter();
    EndlessRecyclerOnScrollListener scrollListener;


    public ResultListFragment() {

    }

    @Override
    public ResultListPresenter createPresenter() {
        return new ResultListPresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_result_list;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContent();

        tvDroneName.setText(drone_name);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        scrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadData(false);
            }
        };
        rvResultDrone.addOnScrollListener(scrollListener);
        rvResultDrone.setHasFixedSize(true);
        rvResultDrone.setLayoutManager(layoutManager);
        rvResultDrone.setAdapter(resAdapter);
        loadData(false);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                    return true;
                }
                return false;
            }
        });

        ItemClickSupport.addTo(rvResultDrone).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                String date = CommonUtilities.getFormatedDate(resAdapter.getItem(position).getCreateAt(), "yyyy-MM-dd hh:mm:ss", "dd MMM yyyy hh:mm");
                sweetAlertDialogSweet("~HASIL~",
                        "Nomor unik : "+resAdapter.getItem(position).getAuthorId()+""+resAdapter.getItem(position).getDroneId()+""+resAdapter.getItem(position).getResultId()+"\n"+
                                "Ditest pada : "+date+"\n"+
                                "Kecepatan rata2 : "+resAdapter.getItem(position).getSpeedAverage()+"\n"+
                                "Batery awal : "+resAdapter.getItem(position).getBateryStart()+"\n"+
                                "Batery akhir : "+resAdapter.getItem(position).getBateryStop()+"\n"+
                                "Waktu test : "+resAdapter.getItem(position).getTimerTest(),
                        "Selesai", null, false,SweetAlertDialog.NORMAL_TYPE)
                        .setConfirmClickListener(sweetAlertDialog->{sweetAlertDialog.dismissWithAnimation();}).show();
            }
        });

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(RequestBase data) {
        resAdapter.setItems(data.getResultList());
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        if (pullToRefresh) {
            resAdapter.clear();
            contentView.setRefreshing(false);
        }
        int page = resAdapter.getItemCount();
        presenter.loadResultList(drone_id, page);
    }
}