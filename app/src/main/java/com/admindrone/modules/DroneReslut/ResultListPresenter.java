package com.admindrone.modules.DroneReslut;

import android.content.Context;

import com.admindrone.app.App;
import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.ABase.RequestBase;

import java.util.HashMap;

public class ResultListPresenter extends BaseRxLcePresenter<XResultListView, RequestBase>
        implements XResultListPresenter {

    public ResultListPresenter(Context context) {
        super(context);
    }

    public void loadResultList(String drone_id, int page) {
        HashMap<String, String> map = new HashMap<>();
        map.put("act", "drone_result_list");
        map.put("drone_id", drone_id);
        map.put("page", page+"");
        subscribe(App.getService().apiDroneResultList(map));
    }
}