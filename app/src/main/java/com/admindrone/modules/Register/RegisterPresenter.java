package com.admindrone.modules.Register;

import android.content.Context;

import com.admindrone.app.App;
import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.ABase.RequestBase;
import com.admindrone.utility.Logs;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RegisterPresenter extends BaseRxLcePresenter<XRegisterView, RequestBase>
        implements XRegisterPresenter {

    public RegisterPresenter(Context context) {
        super(context);
    }

    public void createUser(String nama, String noHp, File actualImage) {
        RequestBody act = RequestBody.create(MediaType.parse("text/plain"), "register");
        RequestBody author_name = RequestBody.create(MediaType.parse("text/plain"), nama);
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), noHp);

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), actualImage);
        MultipartBody.Part file = MultipartBody.Part.createFormData("author_image", actualImage.getName(), requestBody);

        subscribe(App.getService().apiUserRequestRegister(act, phone, author_name, file), false);
    }

    public void createUser(String nama, String noHp) {
        HashMap<String, String> map = new HashMap<>();
        map.put("act", "registernoimage");
        map.put("phone", noHp);
        map.put("author_name", nama);
        subscribe(App.getService().apiUserRequestRegister(map));
    }
}