package com.admindrone.modules.Register;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.admindrone.R;
import com.admindrone.base.BaseMvpLceFragment;
import com.admindrone.models.ABase.RequestBase;
import com.admindrone.modules.MainActivity;
import com.admindrone.utility.CommonUtilities;
import com.admindrone.utility.FunctionMedia;
import com.admindrone.utility.Preferences;
import com.google.android.material.textfield.TextInputEditText;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static android.app.Activity.RESULT_OK;
import static com.admindrone.utility.Config.INTENT_CAMERA;
import static com.admindrone.utility.Config.INTENT_GALLERY;

public class RegisterFragment extends BaseMvpLceFragment<RelativeLayout, RequestBase, XRegisterView, RegisterPresenter>
        implements XRegisterView {

    @BindView(R.id.btnBack)
    Button btnBack;
    @BindView(R.id.rlBar)
    RelativeLayout rlBar;
    @BindView(R.id.ivProfile)
    CircularImageView ivProfile;
    @BindView(R.id.etNamaLengkap)
    TextInputEditText etNamaLengkap;
    @BindView(R.id.etNoHp)
    TextInputEditText etNoHp;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.btnRegister)
    Button btnRegister;

    Uri uriImage;
    File actualImage;
    @BindView(R.id.btnEditGambar)
    Button btnEditGambar;

    private AlertDialog alertDialog ;

    public RegisterFragment() {

    }

    @Override
    public RegisterPresenter createPresenter() {
        return new RegisterPresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_register;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        alertDialog = new SpotsDialog(getContext(), R.style.CustomDialog);
        loadingView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
        onButtonClicked(view);
    }

    @OnClick({R.id.btnBack, R.id.btnRegister, R.id.btnEditGambar})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnBack :
                CommonUtilities.hideSoftKeyboard(getActivity());
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;

            case R.id.btnEditGambar :
                final RxPermissions rxPermissions = new RxPermissions(getActivity());
                PopupMenu popupMenu = new PopupMenu(getContext(), ivProfile);
                popupMenu.inflate(R.menu.menu_upload);
                popupMenu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.mn_camera:
                            rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .subscribe(new Observer<Boolean>() {

                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            if (aBoolean) {
                                                uriImage = FunctionMedia.Camera(RegisterFragment.this, INTENT_CAMERA);
                                            } else {
                                                showToast("Gagal membuka gambar");
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            showToast("Gagal membuka gambar");
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                            break;
                        case R.id.mn_gallery:
                            rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .subscribe(new Observer<Boolean>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            if (aBoolean) {
                                                FunctionMedia.BrowseImage(RegisterFragment.this, INTENT_GALLERY);
                                            } else {
                                                showToast("Permission denied");
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            showToast("Permission denied");
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                            break;
                    }
                    return false;
                });
                popupMenu.show();
                break;

            case R.id.btnRegister :
                if (!etNoHp.getText().toString().isEmpty() && !etNamaLengkap.getText().toString().isEmpty()){
                    alertDialog.show();
                    CommonUtilities.hideSoftKeyboard(getActivity());
                    loadData(false);
                }
                break;
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        alertDialog.dismiss();
        return e.getMessage();
    }

    @Override
    public void setData(RequestBase data) {
        alertDialog.dismiss();
        if (data.getResultAdd().equals("success")){
            CommonUtilities.hideSoftKeyboard(getActivity());
            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.popBackStack();
        } else {
            sweetAlertDialogSweet("GAGAL MEMBUAT AKUN","Pastikan E-mail belum pernah didaftarkan sebelumnya",
                    "Oke", null, false,SweetAlertDialog.ERROR_TYPE)
                    .setConfirmClickListener(sweetAlertDialog->{sweetAlertDialog.dismissWithAnimation();}).show();
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        if (uriImage != null){
            actualImage = new File(uriImage.getPath());
            presenter.createUser(etNamaLengkap.getText().toString(),etNoHp.getText().toString(), actualImage);
        } else {
            presenter.createUser(etNamaLengkap.getText().toString(),etNoHp.getText().toString());
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == INTENT_CAMERA) {
                CropImage.activity(uriImage)
                        .setAspectRatio(4, 4)
                        .setFixAspectRatio(true)
                        .start(getContext(), RegisterFragment.this);
            } else if (requestCode == INTENT_GALLERY) {
                uriImage = data.getData();
                CropImage.activity(uriImage)
                        .setAspectRatio(4, 4)
                        .setFixAspectRatio(true)
                        .start(getContext(), RegisterFragment.this);
            }
        } else {
            if (requestCode == INTENT_CAMERA) {
                showLightError("Gagal mengambil gambar.");
            } else if (requestCode == INTENT_GALLERY) {
                showLightError("Gagal mengambil gambar.");
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriImage = result.getUri();
                Picasso.get().load(uriImage).into(ivProfile);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                showLightError(error.getMessage());
            }
        }
    }

}