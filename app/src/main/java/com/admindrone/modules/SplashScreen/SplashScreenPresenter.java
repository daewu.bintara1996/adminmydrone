package com.admindrone.modules.SplashScreen;

import android.content.Context;

import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.ABase.RequestBase;

public class SplashScreenPresenter extends BaseRxLcePresenter<XSplashScreenView, RequestBase>
        implements XSplashScreenPresenter {

    public SplashScreenPresenter(Context context) {
        super(context);
    }

}