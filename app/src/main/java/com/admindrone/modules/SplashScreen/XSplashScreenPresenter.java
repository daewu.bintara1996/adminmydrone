package com.admindrone.modules.SplashScreen;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XSplashScreenPresenter extends MvpPresenter<XSplashScreenView> {

}