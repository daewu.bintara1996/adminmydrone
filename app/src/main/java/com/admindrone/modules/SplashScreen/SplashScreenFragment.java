package com.admindrone.modules.SplashScreen;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.admindrone.R;
import com.admindrone.base.BaseMvpLceFragment;
import com.admindrone.models.ABase.RequestBase;
import com.admindrone.modules.Home.HomeFragmentBuilder;
import com.admindrone.modules.Login.LoginPageFragment;
import com.admindrone.utility.FragmentStack;
import com.admindrone.utility.Preferences;

import butterknife.BindView;

import static com.admindrone.BuildConfig.VERSION_NAME;
import static com.admindrone.utility.Config.TOKEN;

public class SplashScreenFragment extends BaseMvpLceFragment<RelativeLayout, RequestBase, XSplashScreenView, SplashScreenPresenter>
        implements XSplashScreenView {

    FragmentStack fragmentStack;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.container2)
    FrameLayout container2;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    private String onToken = "";
    private Handler handler = new Handler();
    private String author_id;

    public SplashScreenFragment() {

    }

    @Override
    public SplashScreenPresenter createPresenter() {
        return new SplashScreenPresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_splash_screnn;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Preferences preferences = new Preferences(getContext());
        onToken = preferences.getPreferencesString(TOKEN);
        author_id = preferences.getPreferencesString("author_id");
        tvVersion.setText("Version "+VERSION_NAME);
        if (onToken != null) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mListener.gotoPage(new HomeFragmentBuilder(author_id).build(), true, "home");
                }
            }, 1000);
        } else {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mListener.gotoPage(new LoginPageFragment(), true, "login");
                }
            }, 1000);
        }
        loadData(false);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(RequestBase data) {

    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}