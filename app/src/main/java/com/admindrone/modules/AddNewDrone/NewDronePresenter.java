package com.admindrone.modules.AddNewDrone;

import android.content.Context;

import com.admindrone.app.App;
import com.admindrone.base.BaseRxLcePresenter;
import com.admindrone.models.ABase.RequestBase;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class NewDronePresenter extends BaseRxLcePresenter<XNewDroneView, RequestBase>
        implements XNewDronePresenter {

    public NewDronePresenter(Context context) {
        super(context);
    }

    public void addNewDrone(String drone_name, String author_id, File actualImage, String reg_id) {
        RequestBody act, drone_reg_id, drone_author_id, drone_nameq;
        act = RequestBody.create(MediaType.parse("text/plain"), "add_drone");
        drone_author_id = RequestBody.create(MediaType.parse("text/plain"), author_id);
        drone_reg_id = RequestBody.create(MediaType.parse("text/plain"), reg_id);
        drone_nameq = RequestBody.create(MediaType.parse("text/plain"), drone_name);

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), actualImage);
        MultipartBody.Part drone_image = MultipartBody.Part.createFormData("drone_image", actualImage.getName(), requestBody);

        subscribe(App.getService().apiAddNewDrone(act, drone_author_id, drone_nameq, drone_reg_id, drone_image), false);

    }
}