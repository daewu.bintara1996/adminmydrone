package com.admindrone.modules.AddNewDrone;

import com.admindrone.models.ABase.RequestBase;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XNewDroneView extends MvpLceView<RequestBase> {

}