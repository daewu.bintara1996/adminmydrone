package com.admindrone.modules.AddNewDrone;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XNewDronePresenter extends MvpPresenter<XNewDroneView> {

}