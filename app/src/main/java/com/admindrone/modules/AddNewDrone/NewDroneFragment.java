package com.admindrone.modules.AddNewDrone;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.admindrone.R;
import com.admindrone.base.BaseMvpLceFragment;
import com.admindrone.models.ABase.RequestBase;
import com.admindrone.modules.Home.HomeFragment;
import com.admindrone.modules.Home.HomeFragmentBuilder;
import com.admindrone.utility.FunctionMedia;
import com.google.android.material.textfield.TextInputEditText;

import com.google.firebase.iid.FirebaseInstanceId;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static android.app.Activity.RESULT_OK;
import static com.admindrone.utility.Config.INTENT_CAMERA;
import static com.admindrone.utility.Config.INTENT_GALLERY;

@FragmentWithArgs
public class NewDroneFragment extends BaseMvpLceFragment<RelativeLayout, RequestBase, XNewDroneView, NewDronePresenter>
        implements XNewDroneView {

    @Arg String author_id;

    @BindView(R.id.btnBack)
    Button btnBack;
    @BindView(R.id.rlBar)
    RelativeLayout rlBar;
    @BindView(R.id.ivProfile)
    CircularImageView ivProfile;
    @BindView(R.id.btnEditGambar)
    Button btnEditGambar;
    @BindView(R.id.etDroneName)
    TextInputEditText etDroneName;
    @BindView(R.id.btnAddDrone)
    Button btnAddDrone;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    private Uri uriImage;
    private String reg_id;


    public NewDroneFragment() {

    }

    @Override
    public NewDronePresenter createPresenter() {
        return new NewDronePresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_new_drone;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContent();
        reg_id = FirebaseInstanceId.getInstance().getToken();

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                    return true;
                }
                return false;
            }
        });

        OnClickView(view);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(RequestBase data) {
        if (data.getResultAdd().equals("success")){
            showToast("Berhasil menambahkan drone");
            mListener.gotoPage(new HomeFragmentBuilder(author_id).build(), true, "home");
        } else {
            showToast("Gagal menambahkan drone");
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        File actualImage = new File(uriImage.getPath());
        presenter.addNewDrone(etDroneName.getText().toString(), author_id, actualImage, reg_id);
    }

    @OnClick({R.id.btnAddDrone, R.id.btnEditGambar, R.id.btnBack})
    public void OnClickView(View view){
        switch (view.getId()){
            case R.id.btnBack:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;
            case R.id.btnEditGambar :
                final RxPermissions rxPermissions = new RxPermissions(getActivity());
                PopupMenu popupMenu = new PopupMenu(getContext(), ivProfile);
                popupMenu.inflate(R.menu.menu_upload);
                popupMenu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.mn_camera:
                            rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .subscribe(new Observer<Boolean>() {

                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            if (aBoolean) {
                                                uriImage = FunctionMedia.Camera(NewDroneFragment.this, INTENT_CAMERA);
                                            } else {
                                                showToast("Gagal membuka gambar");
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            showToast("Gagal membuka gambar");
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                            break;
                        case R.id.mn_gallery:
                            rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .subscribe(new Observer<Boolean>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            if (aBoolean) {
                                                FunctionMedia.BrowseImage(NewDroneFragment.this, INTENT_GALLERY);
                                            } else {
                                                showToast("Permission denied");
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            showToast("Permission denied");
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                            break;
                    }
                    return false;
                });
                popupMenu.show();
                break;
            case R.id.btnAddDrone :
                if (!etDroneName.getText().toString().isEmpty() && uriImage != null){
                    loadData(false);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == INTENT_CAMERA) {
                CropImage.activity(uriImage)
                        .setAspectRatio(4, 4)
                        .setFixAspectRatio(true)
                        .start(getContext(), NewDroneFragment.this);
            } else if (requestCode == INTENT_GALLERY) {
                uriImage = data.getData();
                CropImage.activity(uriImage)
                        .setAspectRatio(4, 4)
                        .setFixAspectRatio(true)
                        .start(getContext(), NewDroneFragment.this);
            }
        } else {
            if (requestCode == INTENT_CAMERA) {
                showLightError("Gagal mengambil gambar.");
            } else if (requestCode == INTENT_GALLERY) {
                showLightError("Gagal mengambil gambar.");
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriImage = result.getUri();
                Picasso.get().load(uriImage).into(ivProfile);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                showLightError(error.getMessage());
            }
        }
    }

}