package com.admindrone.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {
    @SerializedName("author_id")
    @Expose
    private Integer authorId;
    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("author_phone")
    @Expose
    private String authorPhone;
    @SerializedName("author_image")
    @Expose
    private String authorImage;
    @SerializedName("author_reg_id")
    @Expose
    private String authorRegId;
    @SerializedName("create_at")
    @Expose
    private String createAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorPhone() {
        return authorPhone;
    }

    public void setAuthorPhone(String authorPhone) {
        this.authorPhone = authorPhone;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(String authorImage) {
        this.authorImage = authorImage;
    }

    public String getAuthorRegId() {
        return authorRegId;
    }

    public void setAuthorRegId(String authorRegId) {
        this.authorRegId = authorRegId;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }



}
