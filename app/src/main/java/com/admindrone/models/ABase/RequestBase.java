package com.admindrone.models.ABase;

import com.admindrone.models.Register;
import com.admindrone.models.ResultList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestBase {
    @SerializedName("register")
    @Expose
    private Register register;

    @SerializedName("result_add")
    @Expose
    private String resultAdd;

    @SerializedName("result_list")
    @Expose
    private List<ResultList> resultList = null;

    public List<ResultList> getResultList() {
        return resultList;
    }

    public void setResultList(List<ResultList> resultList) {
        this.resultList = resultList;
    }


    public String getResultAdd() {
        return resultAdd;
    }

    public void setResultAdd(String resultAdd) {
        this.resultAdd = resultAdd;
    }

    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }
}
