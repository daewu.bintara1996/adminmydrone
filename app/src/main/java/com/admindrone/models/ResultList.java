package com.admindrone.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultList {

    @SerializedName("result_id")
    @Expose
    private String resultId;
    @SerializedName("author_id")
    @Expose
    private String authorId;
    @SerializedName("drone_id")
    @Expose
    private String droneId;
    @SerializedName("create_at")
    @Expose
    private String createAt;
    @SerializedName("speed_average")
    @Expose
    private String speedAverage;
    @SerializedName("batery_start")
    @Expose
    private String bateryStart;
    @SerializedName("batery_stop")
    @Expose
    private String bateryStop;

    @SerializedName("timer_test")
    @Expose
    private String timerTest;

    public String getTimerTest() {
        return timerTest;
    }

    public void setTimerTest(String timerTest) {
        this.timerTest = timerTest;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getDroneId() {
        return droneId;
    }

    public void setDroneId(String droneId) {
        this.droneId = droneId;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getSpeedAverage() {
        return speedAverage;
    }

    public void setSpeedAverage(String speedAverage) {
        this.speedAverage = speedAverage;
    }

    public String getBateryStart() {
        return bateryStart;
    }

    public void setBateryStart(String bateryStart) {
        this.bateryStart = bateryStart;
    }

    public String getBateryStop() {
        return bateryStop;
    }

    public void setBateryStop(String bateryStop) {
        this.bateryStop = bateryStop;
    }

}