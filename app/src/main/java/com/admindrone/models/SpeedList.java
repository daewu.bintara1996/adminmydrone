package com.admindrone.models;

public class SpeedList {
    public String speed;
    public Double doubleSpeed;

    public void SpeedList(){}

    public Double getDoubleSpeed() {
        return doubleSpeed;
    }

    public void setDoubleSpeed(Double doubleSpeed) {
        this.doubleSpeed = doubleSpeed;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
