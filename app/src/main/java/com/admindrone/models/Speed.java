package com.admindrone.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Speed {
    public String speed;
    public String longitude;
    public String latitude;
    public String batteryStart;

    public Speed(){

    }

    public String getBatteryStart() {
        return batteryStart;
    }

    public void setBatteryStart(String batteryStart) {
        this.batteryStart = batteryStart;
    }

    public String getBatteryStop() {
        return batteryStop;
    }

    public void setBatteryStop(String batteryStop) {
        this.batteryStop = batteryStop;
    }

    public String batteryStop;

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Speed(String speed) {
        this.speed = speed;
    }

}
