package com.admindrone.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestDrone {
    @SerializedName("action")
    @Expose
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("author_image")
    @Expose
    private String authorImage;
    @SerializedName("result")
    @Expose
    private List<DroneList> result = null;


    @SerializedName("result_add")
    @Expose
    private String resultAdd;

    public String getResultAdd() {
        return resultAdd;
    }

    public void setResultAdd(String resultAdd) {
        this.resultAdd = resultAdd;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(String authorImage) {
        this.authorImage = authorImage;
    }

    public List<DroneList> getDroneList() {
        return result;
    }

    public void setDroneList(List<DroneList> result) {
        this.result = result;
    }

}
