package com.admindrone.utility;

import com.admindrone.BuildConfig;

import okhttp3.logging.HttpLoggingInterceptor;

public class Config {

    public static final String BASE_URL = "http://apidrone.hubbyt.site/";
    public static final boolean DEBUG_MODE = true;
    static final String APP_NAME = ".-AndroidBaseApp-.";
    static final String LOG_TAG = ".-AndroidBaseApp-.";
    public static final String GOOGLE_SIGN_IN_API = ".-AndroidBaseApp-.";
    static final String AUTH_KEY = "49b3aa4b7e9732b75e4a4908994822fb";

    public static final String TOKEN = "TOKEN";

    //Konstanta
    public static final int NOTIFICATION_ID = 575855;
    public static final int NOTIFICATION_MESSAGE_ID = 575856;

    public static final int INTENT_CAMERA =2;
    public static final int INTENT_GALLERY =3;


    // Json data notification firebase
    public static final String JSON_DATA_NOTIFICATION = "json_data_notification";

    public static HttpLoggingInterceptor.Level getLogLevel() {
        if (BuildConfig.DEBUG) {
            return HttpLoggingInterceptor.Level.BODY;
        }else{
            return HttpLoggingInterceptor.Level.NONE;
        }
    }
}
