package com.admindrone.utility;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static com.admindrone.BuildConfig.APP_NAME;


public class FunctionMedia {
    public static String APP_FILE_PROFIDER = "com.mydrone";

    public static Uri getTarget(Context context) {
        String cameraFileName = getTargetImage();
        return FileProvider.getUriForFile(context, APP_FILE_PROFIDER, new File(cameraFileName));
    }
    public static String getTargetImage() {
        String directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + APP_NAME;
        File dirFile = new File(directory);
        if (!dirFile.exists()) {
            if (dirFile.mkdir()) {
                Log.i("Directory Image", "directory created");
            } else {
                Log.i("Directory Image", "Can't create directory");
            }
        }
        return directory + "/IMG-" + System.currentTimeMillis() + "-" + APP_NAME + ".jpeg";
    }
    public static void RequestPermission(Activity activity, int code){
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                code);
    }
    public static void startCrop(Uri uri, Activity activity, int REQ_CODE) {

//        UCrop.of(uri, Uri.fromFile(new File(getTargetImage())))
//                .withMaxResultSize(600, 732)
//                .start(activity, REQ_CODE);
    }
    public static void startCrop(Uri uri, Activity activity, Fragment f, int REQ_CODE) {
//        Context context;
//        if (activity!=null){
//            context = activity.getBaseContext();
//        }else {
//            context = f.getContext();
//        }
//        UCrop.Options options = new UCrop.Options();
//        options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary));
//        options.setToolbarColor(ContextCompat.getColor(context,R.color.colorPrimary));
//        options.setToolbarWidgetColor(ContextCompat.getColor(context,R.color.white));
//        UCrop.of(uri, Uri.fromFile(new File(getTargetImage())))
//                .withMaxResultSize(600, 732)
//                .withOptions(options)
//                .start(activity, f,REQ_CODE);
    }
    public static void BrowseImage(Activity activity, int code){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent,"Select Picture"), code);
    }
    public static void BrowseImage(Fragment f, int code){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        f.startActivityForResult(Intent.createChooser(intent,"Select Picture"), code);
    }
    public static Uri Camera(Activity activity , int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE){
        String fileName = getTargetImage();
        Uri imageUri;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        values.put(MediaStore.Images.Media.DESCRIPTION,"Images");
        imageUri = activity.getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION| Intent.FLAG_GRANT_READ_URI_PERMISSION);
        activity.startActivityForResult( intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        return imageUri;
    }
    public static Uri Camera(Fragment f , int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE){
        String fileName = getTargetImage();
        Uri imageUri;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        values.put(MediaStore.Images.Media.DESCRIPTION,"Images");
        imageUri = f.getActivity().getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION| Intent.FLAG_GRANT_READ_URI_PERMISSION);
        f.startActivityForResult( intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        return imageUri;
    }
    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
