package com.admindrone.adapter;

import com.admindrone.base.BaseRecyclerViewAdapter;
import com.admindrone.holder.ItemResultHolder;
import com.admindrone.models.ResultList;

public class ResultDroneAdapter extends BaseRecyclerViewAdapter<ResultList, ItemResultHolder> {

    public ResultDroneAdapter() {
        super(ItemResultHolder.class);
    }

    @Override
    protected void populateViewHolder(ItemResultHolder viewHolder, ResultList item, int position) {
        viewHolder.bindView(item);
    }
}