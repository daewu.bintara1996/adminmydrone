package com.admindrone.adapter;

import com.admindrone.base.BaseRecyclerViewAdapter;
import com.admindrone.holder.ItemDroneHolder;
import com.admindrone.models.DroneList;

public class DronelistAdapter extends BaseRecyclerViewAdapter<DroneList, ItemDroneHolder> {

    public DronelistAdapter() {
        super(ItemDroneHolder.class);
    }

    @Override
    protected void populateViewHolder(ItemDroneHolder viewHolder, DroneList item, int position) {
        viewHolder.bindView(item);
    }
}