package com.admindrone.apis;

import com.admindrone.models.ABase.RequestBase;
import com.admindrone.models.Login;
import com.admindrone.models.RequestDrone;
import com.admindrone.models.VerivyOtp;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

   @FormUrlEncoded
   @POST("public/api/drone/author")
   Observable<Login> apiLoginRequest(@FieldMap Map<String, String> params);

   @FormUrlEncoded
   @POST("public/api/drone/author")
   Observable<RequestBase> apiUserRequestRegister(@FieldMap Map<String, String> params);

   @Multipart
   @POST("public/api/drone/author")
   Observable<RequestBase> apiUserRequestRegister(
           @Part("act") RequestBody act,
           @Part("phone") RequestBody phone,
           @Part("author_name") RequestBody author_name,
           @Part MultipartBody.Part author_image
    );

   @FormUrlEncoded
   @POST("public/api/drone/author")
   Observable<VerivyOtp> apiVerifyOtpRequest(@FieldMap Map<String, String> params);

   @FormUrlEncoded
   @POST("public/api/drone/drone")
   Observable<RequestDrone> apiDroneRequest(@FieldMap Map<String, String> params);

   @FormUrlEncoded
   @POST("public/api/drone/drone")
   Observable<RequestBase> apiDroneNewResult(@FieldMap Map<String, String> params);

   @FormUrlEncoded
   @POST("public/api/drone/drone")
   Observable<RequestDrone> apiDroneDelete(@FieldMap Map<String, String> params);

   @FormUrlEncoded
   @POST("public/api/drone/drone")
   Observable<RequestBase> apiDroneResultList(@FieldMap Map<String, String> params);

   @FormUrlEncoded
   @POST("public/api/drone/author")
   Observable<RequestDrone> apiRequestSaveRegId(@FieldMap Map<String, String> params);

   @Multipart
   @POST("public/api/drone/drone")
   Observable<RequestBase> apiAddNewDrone(
           @Part("act") RequestBody act,
           @Part("drone_author_id") RequestBody drone_author_id,
           @Part("drone_name") RequestBody drone_name,
           @Part("drone_reg_id") RequestBody drone_reg_id,
           @Part MultipartBody.Part drone_image
   );

   @Multipart
   @POST("public/api/drone/author")
   Observable<RequestDrone> apiUpdateImage(
           @Part("act") RequestBody act,
           @Part("author_id") RequestBody author_id,
           @Part MultipartBody.Part image
   );
}
