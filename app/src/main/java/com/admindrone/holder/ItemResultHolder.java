package com.admindrone.holder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.admindrone.R;
import com.admindrone.models.ResultList;
import com.admindrone.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemResultHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDateTime)
    TextView tvDateTime;

    public ItemResultHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_drone_result, parent, false));
    }

    public ItemResultHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(ResultList item) {
        String date = CommonUtilities.getFormatedDate(item.getCreateAt(), "yyyy-MM-dd hh:mm:ss", "dd MMM yyyy hh:mm");

        tvDateTime.setText(date);

    }
}