package com.admindrone.holder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.admindrone.R;
import com.admindrone.models.DroneList;
import com.admindrone.utility.CommonUtilities;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemDroneHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imgDrone)
    ImageView imgDrone;
    @BindView(R.id.tvDroneTitle)
    TextView tvDroneTitle;
    @BindView(R.id.tvDroneCreateAt)
    TextView tvDroneCreateAt;
    @BindView(R.id.tvStTested)
    TextView tvStTested;
    @BindView(R.id.tvStNonTested)
    TextView tvStNonTested;
    @BindView(R.id.tvDroneStatus)
    TextView tvDroneStatus;
    @BindView(R.id.cvDrone)
    CardView cvDrone;
    @BindView(R.id.tvStatusOnlineDrone)
    TextView tvStatusOnlineDrone;

    public ItemDroneHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_drone_list, parent, false));
    }

    public ItemDroneHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(DroneList item) {
        tvDroneTitle.setText(item.getDroneName());
        String date = CommonUtilities.getFormatedDate(item.getCreateAt(), "yyyy-MM-dd hh:mm:ss", "dd MMM yyyy");
        tvDroneCreateAt.setText(date);
        Picasso.get()
                .load(item.getDroneImage())
                .into(imgDrone);
        switch (item.getDroneStatus()) {
            case "tested":
                tvStTested.setVisibility(View.VISIBLE);
                tvStNonTested.setVisibility(View.GONE);
                break;
            case "nontested":
                tvStTested.setVisibility(View.GONE);
                tvStNonTested.setVisibility(View.VISIBLE);
                break;
        }
        tvDroneStatus.setText(item.getDroneStatus().toUpperCase());

        switch (item.getDroneOnlineStatus()){
            case "online":
                tvStatusOnlineDrone.setBackgroundResource(R.color.colorGreen);
                tvStatusOnlineDrone.setText(R.string.online_down);
                break;
            case "offline":
                tvStatusOnlineDrone.setBackgroundResource(R.color.md_grey_400);
                tvStatusOnlineDrone.setText(R.string.offline_down);
                break;
        }

    }
}