package com.admindrone.listeners;


import androidx.fragment.app.Fragment;

public interface FragmentInteractionListener {
    void gotoPage(Fragment page, boolean shouldReplace, String tagName);
    void back();
}
