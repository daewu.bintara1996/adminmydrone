package com.admindrone.listeners;

public interface AlertListener {
    public void onCancel(String action);
    public void onSubmit(String action);
}
