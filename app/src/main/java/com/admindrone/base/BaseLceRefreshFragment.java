package com.admindrone.base;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;

import com.admindrone.R;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public abstract class BaseLceRefreshFragment<CV extends View, M, V extends MvpLceView<M>, P extends MvpPresenter<V>>
        extends BaseMvpLceFragment<SwipeRefreshLayout, M, V, P> implements SwipeRefreshLayout.OnRefreshListener {

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contentView.setOnRefreshListener(this);
//        int[] colors = getActivity().getResources().getIntArray(R.array.loading_colors);
//        contentView.setColorSchemeColors(colors);
    }

    @Override public void onRefresh() {
        loadData(true);
    }

    @Override public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    @Override public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
    }

    @Override public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
        if (pullToRefresh && !contentView.isRefreshing()) {
            contentView.post(new Runnable() {
                @Override public void run() {
                    contentView.setRefreshing(true);
                }
            });
        }
    }

    @Override public void onDestroyView() {
        contentView.setRefreshing(false);
        super.onDestroyView();
    }

    public SweetAlertDialog sweetAlertDialogSweet(String title,String messages,String btnTextY,String btnTextN, boolean cancelBtn, int ALERTTYPE){
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), ALERTTYPE);
        sweetAlertDialog.setTitleText(title)
                .setContentText(messages).showCancelButton(cancelBtn).setCancelText(btnTextN)
                .setConfirmText(btnTextY);
        return sweetAlertDialog;
    }


    public void ConfirmDialog(String messages,String y,String n,View.OnClickListener clickListener){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_exit);
        if (messages!=null) ((TextView)(dialog.findViewById(R.id.tvMessages))).setText(messages);
        if (y!=null) ((TextView)(dialog.findViewById(R.id.BtnOk))).setText(y);
        if (n!=null) ((TextView)(dialog.findViewById(R.id.btnCancel))).setText(n);
        CompositeOnClickListener x= new CompositeOnClickListener();

        if (clickListener!=null) x.addOnClickListener(clickListener);
        x.addOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.BtnOk).setOnClickListener(x);
        dialog.findViewById(R.id.btnCancel).setOnClickListener(view -> {
            dialog.dismiss();
        });
        dialog.show();
    }

    public void ShowDialogNotif(String messages,String y,View.OnClickListener clickListener){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_notif);
        if (messages!=null) ((TextView)(dialog.findViewById(R.id.tvMessages))).setText(messages);
        if (y!=null) ((TextView)(dialog.findViewById(R.id.BtnOk))).setText(y);
        CompositeOnClickListener x= new CompositeOnClickListener();
        if (clickListener!=null) x.addOnClickListener(clickListener);
        x.addOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.BtnOk).setOnClickListener(x);
        dialog.show();
    }

    private class CompositeOnClickListener implements View.OnClickListener{
        List<View.OnClickListener> listeners;

        public CompositeOnClickListener(){
            listeners = new ArrayList<View.OnClickListener>();
        }

        public void addOnClickListener(View.OnClickListener listener){
            listeners.add(listener);
        }

        @Override
        public void onClick(View v){
            for(View.OnClickListener listener : listeners){
                listener.onClick(v);
            }
        }
    }

}
