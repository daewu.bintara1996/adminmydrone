package com.admindrone.base;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.admindrone.R;
import com.admindrone.listeners.AlertListener;
import com.admindrone.listeners.FragmentInteractionListener;
import com.admindrone.utility.CommonUtilities;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.mapbox.mapboxsdk.Mapbox;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public abstract class BaseMvpLceFragment<CV extends View, M, V extends MvpLceView<M>, P extends MvpPresenter<V>>
        extends MvpLceFragment<CV, M, V, P> {

    protected FragmentInteractionListener mListener;
    private SweetAlertDialog pDialog;
    protected Unbinder unbinder;
    private AlertListener alertListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
    }

    @LayoutRes protected abstract int getLayoutRes();

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);
        Mapbox.getInstance(getContext(), "pk.eyJ1IjoiZGFld3ViaW50YXJhIiwiYSI6ImNqdWhyN2d3ZTA3OWo0NW5iYWZycTNvZXgifQ.Sasogb4izxbOD5dngv4L8Q");
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setAlertListener(AlertListener alertListener) {
        this.alertListener = alertListener;
    }

//    public abstract void showConfirmDialog(int alertType, String title, String content, String confirmLabel, String cancelLabel, String action);

    /**
     * @param alertType    {@link SweetAlertDialog} Alert type Ex: SweetAlertDialog.WARNING_TYPE
     * @param title        Judul popup
     * @param content      Isi popup
     * @param confirmLabel Positive button label
     * @param cancelLabel  Negative button label
     * @param action       Callback action
     * @param cancelable
     */
    public void showConfirmDialog(int alertType, String title, String content, String confirmLabel, String cancelLabel, final String action, boolean cancelable) {
        final SweetAlertDialog alert = CommonUtilities.buildAlert(getActivity(), alertType, title, content, confirmLabel, cancelLabel);
        alert.setCancelable(cancelable);
        alert.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
                if (alertListener != null) {
                    alertListener.onCancel(action);
                }
            }
        });
        alert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
                if (alertListener != null) {
                    alertListener.onSubmit(action);
                }
            }
        });
        alert.show();
    }

    public void showToast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    public void showLoading(String content, boolean cancelable) {
        if (pDialog == null){
            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        }
        pDialog.setTitleText(content);
        pDialog.setCancelable(cancelable);
        pDialog.show();
    }

    public void hideLoading() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public SweetAlertDialog sweetAlertDialogSweet(String title,String messages,String btnTextY,String btnTextN, boolean cancelBtn, int ALERTTYPE){
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), ALERTTYPE);
        sweetAlertDialog.setTitleText(title)
                .setContentText(messages).showCancelButton(cancelBtn).setCancelText(btnTextN)
                .setConfirmText(btnTextY);
        return sweetAlertDialog;
    }


    public void ConfirmDialog(String messages,String y,String n,View.OnClickListener clickListener){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_exit);
        if (messages!=null) ((TextView)(dialog.findViewById(R.id.tvMessages))).setText(messages);
        if (y!=null) ((TextView)(dialog.findViewById(R.id.BtnOk))).setText(y);
        if (n!=null) ((TextView)(dialog.findViewById(R.id.btnCancel))).setText(n);
        BaseMvpLceFragment.CompositeOnClickListener x= new BaseMvpLceFragment.CompositeOnClickListener();

        if (clickListener!=null) x.addOnClickListener(clickListener);
        x.addOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.BtnOk).setOnClickListener(x);
        dialog.findViewById(R.id.btnCancel).setOnClickListener(view -> {
            dialog.dismiss();
        });
        dialog.show();
    }

    public void ShowDialogNotif(String messages,String y,View.OnClickListener clickListener){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_notif);
        if (messages!=null) ((TextView)(dialog.findViewById(R.id.tvMessages))).setText(messages);
        if (y!=null) ((TextView)(dialog.findViewById(R.id.BtnOk))).setText(y);
        BaseMvpLceFragment.CompositeOnClickListener x= new BaseMvpLceFragment.CompositeOnClickListener();
        if (clickListener!=null) x.addOnClickListener(clickListener);
        x.addOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.BtnOk).setOnClickListener(x);
        dialog.show();
    }

    private class CompositeOnClickListener implements View.OnClickListener{
        List<View.OnClickListener> listeners;

        public CompositeOnClickListener(){
            listeners = new ArrayList<View.OnClickListener>();
        }

        public void addOnClickListener(View.OnClickListener listener){
            listeners.add(listener);
        }

        @Override
        public void onClick(View v){
            for(View.OnClickListener listener : listeners){
                listener.onClick(v);
            }
        }
    }


}
