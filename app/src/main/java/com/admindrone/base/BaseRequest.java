package com.admindrone.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseRequest {
    @SerializedName("status")
    @Expose
    protected Boolean status;
    @SerializedName("has_next")
    @Expose
    protected Boolean hasNext;
    @SerializedName("method")
    @Expose
    protected String method;
    @SerializedName("rc")
    @Expose
    protected String rc;
    @SerializedName("text")
    @Expose
    protected String text;

    public Boolean getStatus() {
        return status;
    }

    public Boolean getHasNext() {
        return hasNext;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setHasNext(Boolean hasNext) {
        this.hasNext = hasNext;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }
}