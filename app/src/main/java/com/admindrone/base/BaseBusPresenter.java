package com.admindrone.base;

import android.content.Context;

import com.admindrone.utility.Preferences;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import org.greenrobot.eventbus.EventBus;

public class BaseBusPresenter<V extends MvpView> extends MvpBasePresenter<V> {
    protected Context context;
    protected Preferences prefs;

    public BaseBusPresenter(Context context) {
        if (prefs == null) {
            prefs = new Preferences(context);
        }
        this.context = context;
    }

    public Preferences getPrefs() {
        return prefs;
    }

    @Override public void detachView(boolean retainInstance) {
        if (EventBus.getDefault().isRegistered(getView())) {
            EventBus.getDefault().unregister(getView());
        }
        super.detachView(retainInstance);
        if (!retainInstance) {
        }
    }

    @Override public void detachView() {
        super.detachView();
    }

    @Override public void attachView(V view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(getView())) {
            EventBus.getDefault().register(getView());
        }
    }

}
